def echo(string)
  string
end

def shout(string)
  string.upcase
end

#Stands out as a code sniff, could be an opportunity for refactoring
def repeat(string, num_of_times=nil)
  if num_of_times == nil
    "#{string} #{string}"
  else
    new_string = ""
    num_of_times.times do
      |t| new_string += "#{string}"
      new_string += " " unless t == num_of_times-1
    end
    new_string
  end
end

def start_of_word(string, num_of_letters)
  arr = string.split("")
  new_string = ""
  num_of_letters.times {|ele| new_string += arr[ele] }
  new_string
end

def first_word(string)
  arr = string.split
  arr[0]
end

def titleize(string)
  exceptions = ['and', 'the', 'over']
  titleized_arr = string.split
  titleized_arr.each_index {|ele| titleized_arr[ele]=titleized_arr[ele].capitalize if
    !exceptions.include?(titleized_arr[ele]) || ele==0}
  titleized_arr.join(" ")
end
