def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(num_arr)
  sum = 0
  num_arr.each {|num| sum += num}
  sum
end

#Bonus
def multiply(*nums)
    nums.inject(&:*)
end

def power(base, exponent)
  base ** exponent
end

def factorial(num)
  total = 1
  num.times {|n| total *= (n+1)}
  total 
end
