def translate(string)
  arr=string.split

  if arr.length == 1
    pig_latin_a_word(arr[0])

  else
    arr.length.times {|i| arr[i]=pig_latin_a_word(arr[i])}
    arr.join(" ")
  end
end

def is_vowel?(character)
  ['a','e','i','o','u','A','E','I','O','U'].include?(character)
end

def consonant(arr)
  arr << arr[0]
  arr.reverse!.pop
  arr.reverse!
end

def add_ay(arr)
  arr << "a" << "y"
end

def is_upper?(character)
  character == character.capitalize
end

#Code Sniff here, I think there could be some refactoring here, but
#for some reason struggled with this one more than others
def pig_latin_a_word(string)
  arr = string.split("")
  capital = true if is_upper?(arr[0])
  if is_vowel?(arr[0])
    arr << "a" << "y"

  elsif arr[0].downcase =='q' && arr[1].downcase =='u'
    arr = consonant(arr)
    arr = consonant(arr)
    arr = add_ay(arr)

  elsif arr[1].downcase =='q' && arr[2].downcase =='u'
    arr = consonant(arr)
    arr = consonant(arr)
    arr = consonant(arr)
    arr = add_ay(arr)

  elsif arr[0].downcase =='s' && arr[1].downcase =='c' && arr[2].downcase =='h'
    arr = consonant(arr)
    arr = consonant(arr)
    arr = consonant(arr)
    arr = add_ay(arr)

  elsif !is_vowel?(arr[0]) && !is_vowel?(arr[1]) && !is_vowel?(arr[2])
    arr = consonant(arr)
    arr = consonant(arr)
    arr = consonant(arr)
    arr = add_ay(arr)

  elsif !is_vowel?(arr[0]) && !is_vowel?(arr[1])
    arr = consonant(arr)
    arr = consonant(arr)
    arr = add_ay(arr)

  elsif !is_vowel?(arr[0])
      arr = consonant(arr)
      arr = add_ay(arr)
  end
    arr[0] = arr[0].capitalize if capital == true
    arr.join("")
end
